import React, { useContext } from "react";
import { View, Text, StyleSheet, FlatList, Button } from "react-native";
import BlogContext, { BlogContextState } from "../context/BlogContext";

const IndexScreen = () => {
  const { posts, addBlogPost }: BlogContextState = useContext(BlogContext);

  return (
    <View style={styles.container}>
      <FlatList
        data={posts}
        renderItem={({ item }) => <Text>{item.title}</Text>}
        keyExtractor={(_item, index) => `post_${index}`}
      />
      <Button
        onPress={() => addBlogPost({ title: `Blog post #${posts.length + 1}` })}
        title="Append post"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center"
  }
});

export default IndexScreen;
