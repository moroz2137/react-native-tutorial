import React, { useReducer } from "react";
import { Post } from "../types/post";

export interface BlogContextState {
  posts: Post[];
  addBlogPost(post: Post): void;
}

enum BlogActionType {
  AddPost = "ADD_BLOG_POST"
}

export const BlogContext = React.createContext<BlogContextState>([]);

const initialState: Post[] = [
  { title: "Hello World!" },
  { title: "All your base are belong to us!" },
  { title: "床前明月光" }
];

const blogReducer = (state: Post[], action: any) => {
  switch (action.type) {
    case BlogActionType.AddPost:
      return [...state, action.payload];
  }
};

export const BlogProvider = ({ children }) => {
  const [blogPosts, dispatch] = useReducer(blogReducer, initialState);

  const addBlogPost = (post: Post) => {
    dispatch({
      type: BlogActionType.AddPost,
      payload: post
    });
  };

  const state = {
    posts: blogPosts,
    addBlogPost
  };

  return <BlogContext.Provider value={state}>{children}</BlogContext.Provider>;
};

export default BlogContext;
