import React from "react";
import { Text, StyleSheet, View, Image } from "react-native";

function readableLikes(score) {
  if (score === 1) return "Unu persono ŝatas ĝin.";
  if (score > 1) return `${score} personoj ŝatas ĝin.`;
  return "Neniu ŝatas ĝin.";
}

const ImageDetail = ({ title, imageSource, score }) => {
  return (
    <View>
      <Image source={imageSource} />
      <Text>{title || "Image Detail"}</Text>
      <Text>{readableLikes(score)}</Text>
    </View>
  );
};

const styles = StyleSheet.create({});

export default ImageDetail;
