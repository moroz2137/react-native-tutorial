import React from "react";
import { Text, View, StyleSheet } from "react-native";

const BoxScreen = () => {
  return (
    <View style={styles.container}>
      <View style={[styles.child, styles.child1]}>
        <Text>1</Text>
      </View>
      <View style={[styles.child, styles.child2]}>
        <Text>2</Text>
      </View>
      <View style={[styles.child, styles.child3]}>
        <Text>3</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    display: "flex",
    borderColor: "black",
    borderWidth: 3,
    justifyContent: "space-between",
    height: 168
  },
  child: {
    width: 80,
    height: 80,
    borderWidth: 2
  },
  child2: {
    alignSelf: "flex-end"
  }
});

export default BoxScreen;
