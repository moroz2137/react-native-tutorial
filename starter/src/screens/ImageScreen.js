import React from "react";
import { View, Text, StyleSheet } from "react-native";
import ImageDetail from "../components/ImageDetail";

const ImageScreen = () => {
  return (
    <View>
      <ImageDetail
        title="Arbaro"
        imageSource={require("../../assets/forest.jpg")}
        score={8}
      />
      <ImageDetail
        title="Plaĝo"
        imageSource={require("../../assets/beach.jpg")}
        score={9}
      />
      <ImageDetail
        title="Montano"
        imageSource={require("../../assets/mountain.jpg")}
        score={1}
      />
    </View>
  );
};

const styles = StyleSheet.create({});

export default ImageScreen;
