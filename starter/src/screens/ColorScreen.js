import React, { useState } from "react";
import { View, StyleSheet, Button, FlatList } from "react-native";

const Tile = ({ color }) => (
  <View style={{ height: 100, width: 100, backgroundColor: color }} />
);

function randomRGB() {
  const red = Math.floor(Math.random() * 256);
  const green = Math.floor(Math.random() * 256);
  const blue = Math.floor(Math.random() * 256);
  return `rgb(${red}, ${green}, ${blue})`;
}

const ColorScreen = () => {
  const [colors, setColors] = useState([]);

  const handlePress = () => {
    const newColor = randomRGB();
    setColors([...colors, newColor]);
  };

  return (
    <View>
      <Button title="Add a Color" onPress={handlePress} />
      <FlatList
        style={styles.grid}
        keyExtractor={item => item}
        data={colors}
        renderItem={({ item }) => <Tile color={item} />}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  grid: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap"
  }
});

export default ColorScreen;
