import React, { useReducer } from "react";
import { View, Text, StyleSheet } from "react-native";
import ColorCounter from "../components/ColorCounter";

function toHex(int) {
  if (int > 15 && int < 256) return int.toString(16);
  if (int > 0) return `0${int.toString(16)}`;
  return "00";
}

function toRGB(red, green, blue) {
  return `#${toHex(red)}${toHex(green)}${toHex(blue)}`;
}

function inverse(red, green, blue) {
  return `#${toHex(255 - red)}${toHex(255 - green)}${toHex(255 - blue)}`;
}

const STEP = 8;

const initialState = {
  red: 0,
  green: 0,
  blue: 0
};

function setColor(value, diff) {
  if (value + diff > 255) return 255;
  if (value + diff < 0) return 0;
  return value + diff;
}

const reducer = (state, action) => {
  switch (action.type) {
    case "red":
      return { ...state, red: setColor(state.red, action.payload) };
    case "green":
      return { ...state, green: setColor(state.green, action.payload) };
    case "blue":
      return { ...state, blue: setColor(state.blue, action.payload) };
    default:
      return state;
  }
};

const SquareScreen = () => {
  const [{ red, green, blue }, dispatch] = useReducer(reducer, initialState);

  const rgb = toRGB(red, green, blue);

  return (
    <View>
      <ColorCounter
        onIncrease={() => dispatch({ type: "red", payload: STEP })}
        onDecrease={() => dispatch({ type: "red", payload: -STEP })}
        color="Red"
      />
      <ColorCounter
        color="Green"
        onIncrease={() => dispatch({ type: "green", payload: STEP })}
        onDecrease={() => dispatch({ type: "green", payload: -STEP })}
      />
      <ColorCounter
        color="Blue"
        onIncrease={() => dispatch({ type: "blue", payload: STEP })}
        onDecrease={() => dispatch({ type: "blue", payload: -STEP })}
      />
      <View
        style={{
          backgroundColor: rgb,
          width: "100%",
          height: 100,
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <Text style={{ color: inverse(red, green, blue), fontSize: 25 }}>
          {rgb}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({});

export default SquareScreen;
