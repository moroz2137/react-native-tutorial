import React, { useState } from "react";
import { Text, StyleSheet, View, TextInput } from "react-native";

const TextScreen = () => {
  const [text, setText] = useState("");
  return (
    <View>
      <Text>Enter Name:</Text>
      <TextInput
        style={styles.input}
        autoCapitalize="none"
        autoCorrect={false}
        value={text}
        onChangeText={setText}
      />
      {text ? <Text style={styles.text}>My name is {text}.</Text> : null}
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    margin: 15,
    borderColor: "black",
    fontSize: 18,
    borderWidth: 1,
    padding: 10
  },
  text: {
    textAlign: "center"
  }
});

export default TextScreen;
