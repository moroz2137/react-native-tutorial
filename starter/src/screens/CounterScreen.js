import React, { useReducer } from "react";
import { View, Text, StyleSheet, Button } from "react-native";

const initialState = 0;
const reducer = (state, action) => {
  switch (action.type) {
    case "inc":
      return state + 1;
    case "dec":
      return state - 1;
    default:
      return state;
  }
};

const CounterScreen = () => {
  const [counter, dispatch] = useReducer(reducer, 0);

  return (
    <View>
      <Button title="Increase" onPress={() => dispatch({ type: "inc" })} />
      <Button title="Decrease" onPress={() => dispatch({ type: "dec" })} />
      <Text>Current Count: {counter}</Text>
    </View>
  );
};

const styles = StyleSheet.create({});

export default CounterScreen;
