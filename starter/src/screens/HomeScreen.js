import React from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity } from "react-native";

const HomeScreen = props => {
  const handlePress = to => {
    return () => props.navigation.navigate(to);
  };
  return (
    <View>
      <Text style={styles.text}>無計可施</Text>
      <Button title="Go to List Demo" onPress={handlePress("List")} />
      <Button title="Go to Image Demo" onPress={handlePress("Images")} />
      <Button title="Go to Counter Demo" onPress={handlePress("Counter")} />
      <Button title="Go to Color Demo" onPress={handlePress("Colors")} />
      <Button title="Go to Square Demo" onPress={handlePress("Square")} />
      <Button title="Go to Text Demo" onPress={handlePress("Text")} />
      <Button title="Go to Box Demo" onPress={handlePress("Box")} />
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    textAlign: "center",
    marginTop: 10,
    marginBottom: 10
  }
});

export default HomeScreen;
