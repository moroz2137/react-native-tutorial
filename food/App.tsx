import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import SearchScreen from "./src/screens/SearchScreen";
import DisplayResult from "./src/screens/DisplayResult";

export type RootStackParamList = {
  Search: undefined;
  DisplayResult: { id: string };
};

const Stack = createStackNavigator<RootStackParamList>();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Search">
        <Stack.Screen
          name="Search"
          component={SearchScreen}
          options={{ title: "Search Businesses" }}
        />
        <Stack.Screen name="DisplayResult" component={DisplayResult} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
