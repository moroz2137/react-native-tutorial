import axios from "axios";
import config from "../../secrets";

const instance = axios.create({
  baseURL: "https://api.yelp.com/v3",
  headers: {
    authorization: `Bearer ${config.yelp.apiKey}`
  }
});

export default class {
  static async searchBusinesses(params: { [k: string]: any }) {
    const res = await instance.get("/businesses/search", { params });
    return res.data.businesses;
  }

  static async fetchOne(id: string) {
    const res = await instance.get(`/businesses/${id}`);
    return res.data;
  }
}
