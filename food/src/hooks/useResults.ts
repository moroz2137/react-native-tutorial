import { useState, useEffect } from "react";
import yelp from "../api/yelp";

export default () => {
  const [results, setResults] = useState([]);
  const [error, setError] = useState(null);

  const searchApi = async term => {
    try {
      const res = await yelp.searchBusinesses({
        term,
        limit: 50,
        location: "San Jose"
      });
      setResults(res);
      setError(null);
    } catch (e) {
      setError("Something went wrong.");
    }
  };

  useEffect(() => {
    searchApi("");
  }, []);

  return [searchApi, results, error];
};
