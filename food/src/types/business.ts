export interface Business {
  id: string;
  name: string;
  is_closed: boolean;
  phone: string;
  rating: number;
  image_url: string;
  location: {
    display_address: string[];
  };
  photos: string[];
  price: string;
}
