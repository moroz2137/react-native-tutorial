import React from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity
} from "react-native";
import ResultsDetail from "./ResultsDetail";
import { Business } from "../types/business";
import { useNavigation } from "@react-navigation/native";

interface Props {
  title: string;
  entries: Business[];
  last?: boolean;
}

const ResultsList = ({ title, last, entries }: Props) => {
  const navigation = useNavigation();
  if (!entries.length) return null;
  return (
    <View style={[styles.container, last ? { borderBottomWidth: 0 } : {}]}>
      <Text style={styles.title}>
        {title} ({entries.length})
      </Text>
      <FlatList
        horizontal
        data={entries}
        showsHorizontalScrollIndicator={false}
        keyExtractor={(res: any) => res.id}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={() =>
              navigation.navigate("DisplayResult", { id: item.id })
            }
          >
            <ResultsDetail result={item} />
          </TouchableOpacity>
        )}
        contentContainerStyle={styles.list}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    fontWeight: "bold",
    marginLeft: 15,
    marginBottom: 5
  },
  list: {
    paddingRight: 15
  },
  container: {
    borderBottomColor: "#eee",
    borderBottomWidth: 1,
    marginBottom: 10,
    paddingBottom: 10
  }
});

export default ResultsList;
