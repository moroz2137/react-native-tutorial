import React from "react";
import { View, Text, StyleSheet, TextInput } from "react-native";
import { Feather as Icons } from "@expo/vector-icons";

interface Props {
  term: string;
  onTermChange(term: string): void;
  onSubmit(e: any): void;
}

const SearchBar = ({ term, onTermChange, onSubmit }: Props) => {
  return (
    <View style={styles.background}>
      <Icons name="search" style={styles.iconStyle} />
      <TextInput
        style={styles.inputStyle}
        placeholder="Search"
        value={term}
        autoCapitalize="none"
        autoCorrect={false}
        onEndEditing={() => onSubmit(term)}
        onChangeText={onTermChange}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    backgroundColor: "#f0eeee",
    borderRadius: 5,
    height: 50,
    margin: 15,
    justifyContent: "center",
    flexDirection: "row"
  },
  inputStyle: {
    flex: 1,
    fontSize: 18
  },
  iconStyle: {
    fontSize: 25,
    margin: 10,
    alignSelf: "center"
  }
});

export default SearchBar;
