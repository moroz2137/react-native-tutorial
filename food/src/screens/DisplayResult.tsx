import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import yelp from "../api/yelp";
import { Business } from "../types/business";
import { StackNavigationProp } from "@react-navigation/stack";
import { RootStackParamList } from "../../App";
import { RouteProp } from "@react-navigation/native";
import { FlatList } from "react-native-gesture-handler";

interface Props {
  navigation: StackNavigationProp<RootStackParamList, "DisplayResult">;
  route: RouteProp<RootStackParamList, "DisplayResult">;
}

const DisplayResult = ({ navigation, route }: Props) => {
  console.log(navigation);
  const [result, setResult] = useState<Business | null>(null);

  const getResult = async (id: string) => {
    const res = await yelp.fetchOne(id);
    setResult(res);
    navigation.setOptions({ title: res.name });
  };

  useEffect(() => {
    navigation.setOptions({ title: "" });
    getResult(route.params.id);
  }, []);

  if (!result) return <Text>Loading</Text>;

  return (
    <>
      <View style={styles.imageContainer}>
        <Image style={styles.mainImage} source={{ uri: result.image_url }} />
        <View style={styles.textBg}>
          <Text style={styles.name}>{result.name}</Text>
        </View>
      </View>
      <FlatList
        horizontal
        data={result.photos}
        renderItem={({ item }) => (
          <Image source={{ uri: item }} style={styles.singleImage} />
        )}
        keyExtractor={item => item}
      ></FlatList>
    </>
  );
};

const styles = StyleSheet.create({
  mainImage: {
    width: "100%",
    height: 250
  },
  imageContainer: {
    position: "relative"
  },
  singleImage: {
    height: 100,
    width: 100,
    margin: 10
  },
  name: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 18
  },
  textBg: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    position: "absolute",
    bottom: 15,
    left: 15,
    backgroundColor: "rgba(0,0,0,.5)"
  }
});

export default DisplayResult;
