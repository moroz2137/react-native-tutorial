import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import SearchBar from "../components/SearchBar";
import ResultsList from "../components/ResultsList";
import useResults from "../hooks/useResults";

const filterResultsByPrice = (entries, price) => {
  return entries.filter(entry => entry.price === price);
};

const SearchScreen = () => {
  const [term, setTerm] = useState("");
  const [searchApi, results, error] = useResults();

  return (
    <View style={styles.view}>
      <SearchBar term={term} onTermChange={setTerm} onSubmit={searchApi} />
      <ScrollView>
        <ResultsList
          title="Cost Effective"
          entries={filterResultsByPrice(results, "$")}
        />
        <ResultsList
          title="Bit Pricier"
          entries={filterResultsByPrice(results, "$$")}
        />
        <ResultsList
          title="Big Spender"
          entries={filterResultsByPrice(results, "$$$")}
          last
        />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  view: { backgroundColor: "#fff", flex: 1 }
});

export default SearchScreen;
